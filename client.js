(function() {
    let script = document.createElement('script');
    script.setAttribute('src', 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js');
    document.body.appendChild(script);

    setTimeout(() => {
        let socket  = io.connect('http://localhost:3000');
        let source  = document.documentElement.outerHTML;
        let pattern = /<script.+"http:\/\/api\.solvemedia\.com\/papi\/challenge\.script\?k=(.{30,35})">(?:(?!<script)(?:.|\n))*?<\/script>/;
        let matches = source.match(pattern);
    
        let apiKey = matches[1] || undefined;
    
        if (!apiKey) {
            return;
        }

        socket.emit('captcha new', {
            apiKey: apiKey
        });

        socket.on('captcha solved', data => {
            document.getElementById('adcopy_response').value  = data.adcopy_response;
            document.getElementById('adcopy_challenge').value = data.adcopy_challenge;

            let div = document.createElement('div');
            div.setAttribute('id', 'SuccessClaim');
            document.body.appendChild(div);
        });
    }, 1500);
})();