var app  = require('express')();
var http = require('http').createServer(app);
var io   = require('socket.io')(http);

app.set('views', 'views');
app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/iframe-captcha/:socketId/:apiKey', (req, res) => {
    let url = 'http://api.solvemedia.com/papi/challenge.script?k=';

    res.render('frame', {
        url: url + req.params.apiKey,
        socketId: req.params.socketId,
    });
});

io.on('connection', function(socket) {

    socket.on('captcha new', data => {
        data.socketId = socket.id;
        io.emit('captcha new', data);
    });

    socket.on('captcha solved', data => {
        io.emit('captcha solved', data);
    });

});

http.listen(3000, function() {
    console.log('listening on *:3000');
});